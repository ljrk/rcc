use std::io::Read;
use std::fs::File;
use std::env;

use lrlex::lrlex_mod;
use lrpar::lrpar_mod;
use lrpar::lex::Lexer;
use lrlex::LexerDef;

lrlex_mod!("c99.l");
lrpar_mod!("c99.y");

fn main() {
    let lexerdef = c99_l::lexerdef();
    let args: Vec<String> = env::args().collect();
    let mut f = File::open(&args[1]).unwrap();
    let mut s = String::new();
    f.read_to_string(&mut s). unwrap();
    let input = s;

    for r in lexerdef.lexer(&input).iter() {
        match r {
            Ok(l) => {
                let rule = lexerdef.get_rule_by_id(l.tok_id()).name.as_ref().unwrap();
                let lexeme = &input[l.span().start()..l.span().end()];
                println!("{} {}", rule, lexeme);
            },
            Err(e) => { panic!("{:?}", e); }
        }
    }
}


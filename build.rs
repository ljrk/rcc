use cfgrammar::yacc::YaccKind;
use cfgrammar::yacc::YaccOriginalActionKind;
use lrlex::LexerBuilder;
use lrpar::{CTParserBuilder};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let lex_rule_ids_map = CTParserBuilder::new()
        .yacckind(YaccKind::Original(YaccOriginalActionKind::GenericParseTree))
        .error_on_conflicts(false) // we expect one shift-reduce conflict
        .process_file_in_src("c99.y")?;
    LexerBuilder::new()
        .rule_ids_map(lex_rule_ids_map)
        .process_file_in_src("c99.l")?;
    Ok(())
}
